# swizzle

Swizzle, short for switch shizzle is a utility for auditing switch ports on Juniper switches.   
It checks for total numbers of 10G and 1G ports, and also checks if ports have been down for a long time (default is greater than 28 days).   The idea behind this was to enable a cable tidy-up in cabinets where there may be lots of unused, but still-cabled ports. 

### Requires

* Python3
* Juniper PyEz
* Getpass
* Datetime

## How to use

*  Edit the file to put your list of switches into the 'devices' list.
*  If you want to see how long ports have been unused for, set the number of days you want in lastflaptime
*  Run the script - it will interactively ask for your username and password.
*  The script will create a CSV file called csvfile.csv that contains port counts for you.


## Notes

Please note that this script has not been tested on Juniper switches running ELS software, and as such may not produce results on for these switches.

## Disclaimer
The author accepts no responsibility for the use of this script


