from jnpr.junos import Device
from jnpr.junos.op.phyport import PhyPortTable
from jnpr.junos.exception import ConnectError
from getpass import getpass
import sys, os, time
from datetime import datetime
from datetime import timedelta

junos_username = input("Enter your username: ")
junos_password = getpass(prompt="Enter your password: ")

# List of Junos devices to audit
devices = ['172.16.1.1','172.16.1.2','172.16.1.3']

# Number of days since last flap - used to check for 'unused' ports whose cables could be removed.
lastflaptime = 28

# Calculate the date we should be checking against based on lastflaptime
today = datetime.today()
lastday = today - timedelta(days=lastflaptime)

def getDevicePorts(device):
    try:
        with Device(device, user=junos_username, password=junos_password, port=22) as dev:
            
            # Get the physical port table
            ports = PhyPortTable(dev).get()

            print("=== Looking for ports on {} that are down since before {} ===".format(device, lastday))
            if ports.get:
                portstotal = 0
                Gportstotal = 0
                Gportsup = 0
                Gportsnotup = 0
                Gportsnotuplongtime = 0
                Gportsneverup = 0

                Xportstotal = 0
                Xportsup = 0
                Xportsnotup = 0
                Xportsnotuplongtime = 0
                Xportsneverup = 0

                for port in ports:
                    portstotal = portstotal + 1
                    
                    # Count 1G ports
                    if 'ge-' in port.name:
                        Gportstotal = Gportstotal + 1
                        if port.flapped == 'Never':
                            Gportsneverup = Gportsneverup + 1
                            Gportsnotup = Gportsnotup + 1

                            print(device, port.name, port.oper, ' - has never been up')

                        # If port has been up before (i.e. lastflaptime isn't 'Never')
                        # then check if it is up or down
                        elif port.flapped != 'Never':
                            date = port.flapped
                            date = date.split(' ')
                            date = date[0]
                            testday = port.flapped
                            testday = testday.split(" ")
                            testday = testday[0] + " " + testday[1]
                            testday = datetime.strptime(testday, '%Y-%m-%d %H:%M:%S')
                            if port.oper == "up":
                                Gportsup = Gportsup + 1

                            # If port is not up, check if the port last went down (flapped)
                            # more than the number of days we set in lastflaptime
                            if port.oper != "up":
                                Gportsnotup = Gportsnotup + 1

                                if testday < lastday:
                                    Gportsnotuplongtime = Gportsnotuplongtime + 1
                                    delta = lastday - testday
                                    print(device, port.name, port.oper, 'since',  date, '({} days)'.format(delta.days))
                        # print('\n')
                        
                   # Count 10G ports 
                    if 'xe-' in port.name:
                        Xportstotal = Xportstotal + 1
                        if port.flapped == 'Never':
                            Xportsneverup = Xportsneverup + 1
                            Xportsnotup = Xportsnotup + 1

                            print(device, port.name, port.oper, ' - has never been up')

                        elif port.flapped != 'Never':
                            date = port.flapped
                            date = date.split(' ')
                            date = date[0]
                            testday = port.flapped
                            testday = testday.split(" ")
                            testday = testday[0] + " " + testday[1]
                            testday = datetime.strptime(testday, '%Y-%m-%d %H:%M:%S')
                            if port.oper == "up":
                                Xportsup = Xportsup + 1

                            if port.oper != "up":
                                Xportsnotup = Xportsnotup + 1

                                if testday < lastday:
                                    Xportsnotuplongtime = Xportsnotuplongtime + 1
                                    delta = lastday - testday
                                    print(device, port.name, port.oper, 'since', date, '({} days)'.format(delta.days))


    except ConnectError as err:
        print("Cannot connect to device {}. The error was:  {}".format(device, err))
        sys.exit(1)

    # Print some stats to screen
    print("Total ports found: {}".format(portstotal))
    print("1G  ports found: {}".format(Gportstotal))
    print("10G ports found: {}".format(Xportstotal))
    print("---")
    print("1G  - ports up: {}".format(Gportsup))
    print("1G  - Ports that are not up: {}".format(Gportsnotup))
    print("1G  - Ports that have never been up: {}".format(Gportsneverup))
    print("1G  - Ports long-term unused (more than {} days): {}".format(lastflaptime, Gportsnotuplongtime))
    print("---")
    print("10G - ports up: {}".format(Xportsup))
    print("10G - Ports that are not up: {}".format(Xportsnotup))
    print("10G - Ports that have never been up: {}".format(Xportsneverup))
    print("10G - Ports long-term unused (more than {} days): {}".format(lastflaptime, Xportsnotuplongtime))
    print("---")

    # Write the values to a file
    f = open('csvfile.csv', 'a')
    line = device + ',' + str(portstotal) + ',' + str(Gportstotal) + ',' + str(Xportstotal) + ',' + str(Gportsup) + ',' + str(Xportsup) + ',' + str(Gportsnotup) + ',' + str(Xportsnotup) + ',' + str(Gportsneverup) + ',' + str(Xportsneverup) + ',' + str(Gportsnotuplongtime) + ',' + str(Xportsnotuplongtime) + '\n'
    f.write(line)
    f.close()

def writeHeaders():
    f = open('csvfile.csv', 'w')
    headers = 'Devicename,Total Ports Found,Total 1G Ports,Total 10G Ports,1G Ports Up,10G Ports Up,1G Ports Not Up,10G Ports Not Up, 1G Ports Never Up,10G Ports Never Up, 1G Ports Not Up Long Time, 10G Ports Not Up Long Time\n'
    f.write(headers)
    f.close

def writeCSVUnusedPorts():
    # Write CSV headers for unused port report
    writeHeaders()
    # Now connect to devices in turn and work out what ports are unused, used, never up etc.
    for device in devices:
        getDevicePorts(device)


writeCSVUnusedPorts()